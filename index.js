// console.log("Hello World!");


//[SECTION] Exponent Operator

const firstNum = 8 ** 2; // new
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

//[SECTION] Template Literals

/*
	- allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability

*/

let name = "John";

// Pre-template literals string
// Use single quote or double
let message = "Hello" + name + "! Welcome to programming!";
console.log("Message w/o template literals: \n" + message);


// String Using template literals
// Using backticks (``) (new)
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message w/o template literals: \n ${message}`);



const anotherMessage = `${name} attended a math competition. 
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;
console.log(anotherMessage);

/*
	- template literals allows us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals


*/

const interestRate = .1;
const principal = 100;

console.log(`The interest of your savings account is ${principal * interestRate}`);


//[SECTION] Array Destructuring

/*
	- allows us to unpack an element in an array into distinct variables
	- also allows us name array elements with variables, instead of using index numbers
	- Helps with code readability
	Syntax:
		let/const [variableName, variableName, variableName] = array;

*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre- Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${[fullName[0]]} ${fullName[1]} ${fullName[2]}! It's nice to see you`);

// Array Destructuring(new)

const [firstName, middleName, lastName] = fullName
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`);


// [SECTION] Object Destructuring

/*
	- allows us to unpack properties/keys of objects into distinct variables
	- shortens syntax for accessing properties from objects
	Syntax:
		let/const {propertyVariableName, propertyVariableName, propertyVariableName} = object;
	NOTE - RULE
		the variableName to be assigned in destructuring should be the same as the propertyName
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to see you`);


// Object Destructuring (NEW)

const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you`);



// [SECTION] Arrow Functions (NEW)

/*
	- compact alternative syntax to traditional functions
	- useful for code snippets where creating function will not be reused in any other portion of the code
	- adheres to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name of function that will only be used in certain code snippets
	Syntax:
		const variableName = () => {
		console.log("");
		}

*/

const hello = () => {
	console.log("Hello World!");
}

hello();




const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("John", "D", "Smith");

console.log("-----------------------");
const students = ["John", "Jane", "Judy"];

// Arrow function with loops
// Pre-Arrow function
students.forEach(function(student){
	console.log(`${students} is a student`);

})

// Arrow function in forEach (NEW)
// the function is only used in the forEach method to print out a text with the student's names
students.forEach((student) => {
	console.log(`${student} is a student`);
})


console.log("-------------");



//----------------------------------------------------------------------------






// [SECTION] Implicit Return Function
/*
	- there are instances when you can omit the 'return' statement
	- this works because w/o the 'return' statement JavaScript implicitly adds it for the result of the function
*/


// Pre-Arrow Function

/*(OLD)
const add = (numA, numB) => {
	return numA + numB;
}

let total = add(1,2);
console.log(total);

*/
//(NEW)
const add = (numA, numB) => numA + numB;

let total = add(1,2);
console.log(total);


//[SECTION] Default Function Argument Value
/*
	- provides a default argument value if none is provided when the function is invoked

*/

const greet = (name = 'User') => {
	return `Good morning, ${name}`;
}

console.log(greet("John"));
console.log(greet());


// [SECTION] Class-Based Object Blueprints
/*
	- allows creation/instantaion of objects using classes as blueprints

	Syntax
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}

*/
//(NEW)
class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantating an object
const myCar = new Car();


myCar.brand = "Ford";
myCar.name = "Range Raptor";
myCar.year = 2021

console.log(myCar);


const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);